# ckihook Webhook

## Purpose

This webhook handles CKI for internal contributions.

## Manual Runs

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.ckihook \
        --disable-inactive-branch-check \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1 \
        --action pipeline

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

## Reporting

- Label prefix: `CKI::`
- Label prefix: `CKI_RHEL::`
- Label prefix: `CKI_CentOS::`
- Label prefix: `CKI_Debug::`
- Label prefix: `CKI_ARK::`
- Label prefix: `CKI_ARK_DEBUG::`
- Label prefix: `CKI_ARK_16k::`
- Label prefix: `CKI_ARK_16k_Debug::`
- Label prefix: `CKI_ARK_Clang::`
- Label prefix: `CKI_ARK_Clang_Debug::`
- Label prefix: `CKI_ARK_ClangLto::`
- Label prefix: `CKI_ARK_ClangLto_Debug::`
- Label prefix: `CKI_RT::`
- Label prefix: `CKI_RT_Debug::`
- Label prefix: `CKI_Automotive::`
- Label prefix: `CKI_64k::`
- Label prefix: `CKI_64k_Debug::`
- Label prefix: `CKI_Clang::`
- Label prefix: `CKI_Clang_Debug::`
- Comment header: **CKI Pipelines Status**

The webhook reports the overall result of the check by applying a scoped label
to the MR with the prefix `CKI::`.

Additionally, the webhook will apply a separate label indicating the status of each
expected pipeline for the MR. These possible label prefixes are:

- `CKI_RHEL::` a regular pipeline built in a RHEL environment
- `CKI_CentOS::` a regular pipeline built in a CentOS environment
- `CKI_Debug::` a pipeline that builds kernel-debug
- `CKI_ARK::` a regular pipeline built in a Rawhide environment
- `CKI_ARK_Debug::` a kernel-debug pipeline built in a Rawhide environment
- `CKI_ARK_16k::` a kernel-16k pipeline in a Rawhide environment
- `CKI_ARK_16k_Debug::` a kernel-16k-debug pipeline in a Rawhide environment
- `CKI_ARK_Clang::` a regular pipeline built in a Rawhide environment with clang
- `CKI_ARK_Clang_Debug::` a kernel-debug pipeline built in a Rawhide
  environment with clang
- `CKI_ARK_ClangLto::` a regular pipeline built in a Rawhide environment with
  clang and LTO
- `CKI_ARK_ClangLto_Debug::` a kernel-debug pipeline built in a Rawhide
  environment with clang and LTO
- `CKI_RT::` a pipeline that builds kernel-rt
- `CKI_RT_Debug::` a pipeline that builds kernel-rt-debug
- `CKI_Automotive::` a pipeline that builds kernel-automotive
- `CKI_64k::` a pipeline that builds kernel-64k
- `CKI_64k_Debug::` a pipeline that builds kernel-64k-debug
- `CKI_Clang::` a regular pipeline built with clang
- `CKI_Clang_Debug::` a kernel-debug pipeline built with clang

An MR may have one or more of these labels depending on the expected pipeline
types for the target branch of the MR as defined in the utils/rh_metadata.yml file.

The hook will leave a comment on the MR with the details of the check. The
header of the comment is **CKI Pipelines Status**. This comment will be edited
with updated information every time the hook runs. Refer to the timestamp at the
end of the comment to see when the hook last evaluated the MR.

## Triggering

To trigger reevaluation of an MR by the ckihook webhook either remove any of
the existing `CKI` scoped labels mentioned above or a leave a comment with one
of the following:

- `request-cki-evaluation`
- `request-ckihook-evaluation`

Alternatively, to retrigger all the webhooks at the same time leave a note in
the MR with `request-evaluation`.
