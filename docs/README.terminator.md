# Terminator

## Purpose

In the kernel repositories, [redundant pipelines] are automatically cancelled
by GitLab. The cancelling of the resulting multi-project child pipelines and
associated test runs is handled by this bot.

GitLab jobs can also be cancelled because of timeouts (e.g. test execution is
taking too long). In these cases, the jobs can't clean up the test resources,
and they need to be cleaned up externally.

## Manual runs

You can run the webhook manually on a pipeline repository URL with the command:

    python3 -m webhook.terminator \
        --disable-inactive-branch-check \
        --pipeline-project-url https://gitlab.com/group/pipeline-repo

## Configuration

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

    | Environment variable    | Description                                        |
    |-------------------------|----------------------------------------------------|
    | `CHATBOT_EXCHANGE`      | AMQP exchange to get messages via the chat bot     |
    | `BEAKER_URL`            | Beaker host URL to get clickable links on messages |
    | `BEAKER_OWNER`          | Owner filter to select Beaker jobs                 |
    | `AWS_ACCESS_KEY_ID`     | Access key ID to authenticate to AWS               |
    | `AWS_SECRET_ACCESS_KEY` | AWS secret access key                              |
    | `AWS_DEFAULT_REGION`    | AWS region of the machines                         |

[redundant pipelines]: https://docs.gitlab.com/ee/ci/pipelines/settings.html#auto-cancel-redundant-pipelines
