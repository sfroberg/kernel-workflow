"""Query MRs for upstream commit IDs to compare against submitted patches."""
from dataclasses import dataclass
from dataclasses import field
from os import environ
import re
import sys
import typing

from cki_lib import logger
from cki_lib import misc
# GitPython
import git

from webhook import common
from webhook import defs
from webhook.base_mr import BaseMR
from webhook.base_mr_mixins import CommitsMixin
from webhook.session import new_session

if typing.TYPE_CHECKING:
    from .session import SessionRunner
    from .session_events import GitlabMREvent
    from .session_events import GitlabNoteEvent

LOGGER = logger.get_logger('cki.webhook.fixes')
COMMENT_HEADER = "**Fixes Status:** "


@dataclass
class RHCommit:
    """Per-MR-commit data class for storing comparison data."""

    commit: set
    ucids: set = field(default_factory=set, init=False)
    fixes: str = ""


@dataclass
class Fixes:
    """Per-MR list of fixes and their categorization/status."""

    possible: dict
    in_mr: set
    in_omitted: set
    in_tree: set


@dataclass(repr=False)
class FixesMR(CommitsMixin, BaseMR):
    """Represent the MR and possible Fixes."""

    kernel_src: str = environ.get('LINUX_SRC', '')
    ucids: set = field(default_factory=set, init=False)
    omitted: set = field(default_factory=set, init=False)

    def __post_init__(self):
        """Get the commits and basic MR data."""
        # Load the commits before the basic MR properties (super.__post_init__) because on a large
        # MR loading the commits could take some time so this way the MR state and labels are most
        # up to date once we start doing things with the data.
        self.commits  # pylint: disable=pointless-statement
        super().__post_init__()
        self.rhcommits = {}
        self.ucids = set()
        self.omitted = set()
        self.fixes = Fixes(possible={}, in_mr=set(), in_omitted=set(), in_tree=set())
        self.find_upstream_commit_ids()
        self.check_for_fixes()

    def find_intentionally_omitted_fixes(self, description):
        """Add intentionally omitted fixes hashes to self.omitted set."""
        fixes_pattern = r'^\s*(?P<pfx>Omitted-fix:)[\s]+(?P<hash>[a-f0-9]{8,40})'
        fixes_re = re.compile(fixes_pattern, re.IGNORECASE)
        for line in description.split('\n'):
            gitref = fixes_re.match(line)
            if gitref:
                githash = gitref.group('hash')
                self.omitted.add(githash)
        LOGGER.debug('List of %d Omitted-fixes: %s', len(self.omitted), self.omitted)

    def map_fixes_to_commits(self, repo):
        """Map potential missing fixes to their corresponding MR commits."""
        for ucid, fixes in self.fixes.possible.items():
            for sha, rhcommit in self.rhcommits.items():
                for fix in fixes:
                    if ucid in rhcommit.ucids:
                        rhcommit.fixes += repo.git.log("-1", "--pretty=short", fix)
                        rhcommit.fixes += f'\n    RH-Fixes: {sha[:12]} '
                        rhcommit.fixes += f'("{rhcommit.commit.title}")\n\n'

    def fix_already_in_tree(self, fix, fixee):
        """Check to see if the fix is already included in the tree."""
        our_commits = self.gl_project.search('commits', fix)
        for commit in our_commits:
            if fix in commit['message'] and f'Fixes: {fixee[:8]}' in commit['message']:
                return commit['id']
        return None

    def filter_fixes(self, mapped_fixes):
        """Filter potential fixes against included commits and Omitted-fix refs."""
        for fixee, fixes in mapped_fixes.items():
            LOGGER.info("Found potential missing upstream fixes for upstream commit %s", fixee)
            per_commit_fixes = []
            for fix in fixes:
                propose = True
                committed_as = None
                for ucid in self.ucids:
                    # fix is typically a short hash, ucid is always 40 char hash
                    if ucid.startswith(fix):
                        self.fixes.in_mr.add(ucid)
                        LOGGER.info("Found fix %s for %s in ucids", fix, fixee)
                        propose = False
                        continue
                for omit in self.omitted:
                    # fix is typically a short hash, omit can be 8-40 chars, match first 8 chars
                    if fix[:8] == omit[:8]:
                        self.fixes.in_omitted.add(omit)
                        LOGGER.info("Found fix %s for %s in omitted", fix, fixee)
                        propose = False
                        continue
                if propose:
                    committed_as = self.fix_already_in_tree(fix, fixee)
                    if committed_as is not None:
                        self.fixes.in_tree.add(f'{fix} as {committed_as}')
                        LOGGER.info("Found fix %s already in tree as %s", fix, committed_as)
                        continue
                    LOGGER.info("Found fix %s for %s not included or referenced in MR", fix, fixee)
                    per_commit_fixes.append(fix)
            if per_commit_fixes:
                self.fixes.possible[fixee] = per_commit_fixes

    def find_potential_missing_fixes(self):
        """Store potential missing upstream fixes hashes in fixes_mr.fixes mapping."""
        fixes_pattern = r'^(?P<hash>[a-f0-9]{8,40})'
        fixes_re = re.compile(fixes_pattern, re.IGNORECASE)
        mapped_fixes = {}

        repo = git.Repo(self.kernel_src)

        has_fixes = []
        for ucid in self.ucids:
            # Make sure the commit exists in our tree
            try:
                _ = repo.commit(ucid)
            except ValueError:
                LOGGER.info("Commit ID %s not found in upstream, skipping checks", ucid)
                continue

            fixes = []
            short_hash = ucid[:12]
            fixeslog = repo.git.log("--oneline", f'--grep=Fixes: {short_hash}', f'{short_hash}..')

            if short_hash in fixeslog:
                has_fixes.append(ucid)
                for line in fixeslog.split('\n'):
                    gitref = fixes_re.match(line)
                    if gitref:
                        githash = gitref.group('hash')
                        fixes.append(githash)

            if fixes:
                mapped_fixes[ucid] = fixes

        LOGGER.info("Upstream commits with Fixes: %s", has_fixes)
        self.filter_fixes(mapped_fixes)
        if self.fixes.possible:
            LOGGER.info('Map of potential missing Fixes: %s', self.fixes)
        self.map_fixes_to_commits(repo)

    def extract_ucid(self, commit):
        """Extract upstream commit ID from the message."""
        # pylint: disable=too-many-locals,too-many-branches,too-many-statements
        message_lines = commit.description.text.split('\n')
        gitref_list = []
        # Pattern for 'git show <commit>' (and git log) based backports
        gl_pattern = r'^(?P<pfx>commit) (?P<hash>[a-f0-9]{8,40})$'
        gitlog_re = re.compile(gl_pattern)
        # Pattern for 'git cherry-pick -x <commit>' based backports
        cp_pattern = r'^\((?P<pfx>cherry picked from commit) (?P<hash>[a-f0-9]{8,40})\)$'
        gitcherrypick_re = re.compile(cp_pattern)

        self.find_intentionally_omitted_fixes(commit.description.text)
        for line in message_lines:
            gitref = gitlog_re.match(line) or gitcherrypick_re.match(line)
            if gitref:
                githash = gitref.group('hash')
                hash_prefix = gitref.group('pfx')
                if line == f"{hash_prefix} {githash}" and len(githash) == 40:
                    if githash not in gitref_list:
                        gitref_list.append(githash)
                    self.ucids.add(githash)
                    continue
                if githash not in gitref_list:
                    gitref_list.append(githash)
                self.ucids.add(githash)

        # We return empty arrays if no commit IDs are found
        LOGGER.debug("Found upstream refs: %s", gitref_list)
        return gitref_list

    def find_upstream_commit_ids(self):
        """Check upstream commit IDs."""
        for sha, commit in self.commits.items():
            if sha == self.first_dep_sha:
                LOGGER.info("Skipping dependency commit %s and all the ones after it", sha)
                break
            self.rhcommits[sha] = RHCommit(commit)

        for sha, rhcommit in self.rhcommits.items():
            try:
                found_refs = self.extract_ucid(rhcommit.commit)
            # pylint: disable=broad-except
            except Exception:
                found_refs = []

            rhcommit.ucids = found_refs

        LOGGER.debug('List of %d ucids: %s', len(self.ucids), self.ucids)

    def check_for_fixes(self):
        """Do the thing, check submitted patches vs. referenced upstream commit IDs."""
        LOGGER.debug("Checking for possible missing upstream fixes in MR %s", self.iid)
        self.find_intentionally_omitted_fixes(self.description.text)

        # do NOT run validation if the commit count is over 2000
        if len(self.commits) <= defs.MAX_COMMITS_PER_MR:
            self.find_potential_missing_fixes()

        # append quick label "Fixes" with scope "OK" or "Missing"
        if len(self.fixes.possible) > 0:
            label_status = defs.MISSING_SUFFIX
        else:
            label_status = defs.READY_SUFFIX

        common.add_label_to_merge_request(self.gl_project, self.iid, [f'Fixes::{label_status}'])


def build_fixes_comment(fixes_mr):
    """Build a comment for possible missing Fixes commits."""
    fixes_msg = COMMENT_HEADER
    if fixes_mr.fixes.possible:
        fixes_msg += f'~"Fixes::{defs.MISSING_SUFFIX}"\n'
        fixes_msg += "\nPossible missing Fixes detected upstream:  \n"
        fixes_msg += "```\n"
        for _, rhcommit in fixes_mr.rhcommits.items():
            fixes_msg += rhcommit.fixes
        fixes_msg += "```\n"
        fixes_msg += ("These can be resolved by either backporting and adding the referenced "
                      "commit(s) to your MR, or by adding Omitted-fix: lines to your MR "
                      "description, where appropriate.\n")
    else:
        fixes_msg += f'~"Fixes::{defs.READY_SUFFIX}"\n'
        fixes_msg += f"No missing upstream fixes for MR {fixes_mr.iid} found at this time.\n"

    if fixes_mr.fixes.in_mr:
        fixes_msg += '\nUpstream Fixes included in MR:\n'
        fixes_msg += '\n'.join(f' - {fix}\n' for fix in fixes_mr.fixes.in_mr)

    if fixes_mr.fixes.in_omitted:
        fixes_msg += '\nUpstream Fixes omitted via MR description:\n'
        fixes_msg += '\n'.join(f' - {fix}\n' for fix in fixes_mr.fixes.in_omitted)

    if fixes_mr.fixes.in_tree:
        fixes_msg += '\nUpstream Fixes already in tree:\n'
        fixes_msg += '\n'.join(f' - {fix}\n' for fix in fixes_mr.fixes.in_tree)

    return fixes_msg


def report_results(session, fixes_mr, username, gl_mergerequest):
    """Report the results of our examination."""
    # pylint: disable=too-many-branches
    report = build_fixes_comment(fixes_mr)
    if misc.is_production_or_staging():
        session.update_webhook_comment(gl_mergerequest, report,
                                       bot_name=username,
                                       identifier=COMMENT_HEADER)
    else:
        LOGGER.info('Skipping adding report in non-production\n%s', report)


def get_fixes_mr(session, mr_url):
    """Return a merge request instance for the webhook payload."""
    params = {'graphql': session.graphql,
              'gl_instance': session.gl_instance,
              'projects': session.rh_projects,
              'url': mr_url,
              'kernel_src': session.args.linux_src}
    return FixesMR(**params)


def process_gl_event(
    _: dict,
    session: 'SessionRunner',
    event: typing.Union['GitlabMREvent', 'GitlabNoteEvent'],
    **__: typing.Any
) -> None:
    """Process a gitlab event."""
    LOGGER.info('Processing %s event for %s', event.kind.name, event.mr_url)
    fixes_mr = get_fixes_mr(session, event.mr_url)
    report_results(session, fixes_mr, session.graphql.username, fixes_mr.gl_mr)


HANDLERS = {
    defs.GitlabObjectKind.MERGE_REQUEST: process_gl_event,
    defs.GitlabObjectKind.NOTE: process_gl_event,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('FIXES')
    parser.add_argument('--linux-src',  **common.get_argparse_environ_opts('LINUX_SRC'),
                        help='Directory containing upstream Linux kernel git tree')
    args = parser.parse_args(args)
    if not args.linux_src:
        LOGGER.warning("No Linux source tree directory specified, using default")
    session = new_session('fixes', args, HANDLERS)
    session.run()


if __name__ == "__main__":
    main(sys.argv[1:])
